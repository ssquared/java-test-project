package za.co.ssquared;

import za.co.ssquared.challenge3.Dictionary;

import static za.co.ssquared.util.Assert.isFalse;
import static za.co.ssquared.util.Assert.isTrue;

/**
 * Challenge 3:
 *
 * A word is called a palindrome if word is equal to reverse of word e.g. "mom" is palindrome because reverse of "mom" is "mom".
 *
 * Implement the "isPalindrome()" method is the Dictionary.java, if you run the class and you see "SUCCESS" then you have completed
 * the challenge.
 */
public class Challenge3 {


    public static void main(String[] args) {
        // NOTE: Please DO NOT edit this code. This main method if perfect "as-is", we need you to edit the
        // dictionary.isPalindrome(...) method. Good luck!

        Dictionary dictionary = new Dictionary();

        isTrue(dictionary.isPalindrome("mom"));
        isTrue(dictionary.isPalindrome("omo"));
        isTrue(dictionary.isPalindrome("dad"));
        isTrue(dictionary.isPalindrome("121"));
        isFalse(dictionary.isPalindrome("abc"));
        isTrue(dictionary.isPalindrome("MOM"));
        isTrue(dictionary.isPalindrome("MOM"));
        isTrue(dictionary.isPalindrome("MoM"));
        isFalse(dictionary.isPalindrome("M0M!"));
        isFalse(dictionary.isPalindrome(null));

        System.out.println("SUCCESS");
    }
}
