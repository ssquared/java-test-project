package za.co.ssquared.challenge4;

/**
 * Challenge 4:
 * <p>
 * In fibonacci series, next number is the sum of previous two numbers for example:
 * 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 etc.
 * <p>
 * The first two numbers of fibonacci series are 0 and 1.
 * <p>
 * The challenge is write the fibonacci series program in java using recursion.
 */
public class Fibonacci {

    public Fibonacci() {
    }

    public int calculate(int n) {
        // Write your code here for "Challenge 4". Hint: You need to use recursive method.
        return -1;
    }
}
