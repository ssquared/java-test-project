package za.co.ssquared;

import za.co.ssquared.challenge4.Fibonacci;

import java.util.Scanner;

/**
 * Challenge 4:
 * <p>
 * In fibonacci series, next number is the sum of previous two numbers for example:
 * 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 etc.
 * <p>
 * The first two numbers of fibonacci series are 0 and 1.
 * <p>
 * The challenge is write the fibonacci series program in java using recursion.
 */
public class Challenge4 {


    public static void main(String[] args) {
        // 1. Input the limit to how many numbers you want the Fibonacci series to show.
        System.out.println("Enter number upto which Fibonacci series to print: ");

        // 2. Get the user's input.
        int limit = new Scanner(System.in).nextInt();
        System.out.println("Executing Fibonacci series up to " + limit + " numbers... ");

        // 3. Printing Fibonacci series up to the given limit.
        Fibonacci fibonacci = new Fibonacci();
        for (int i = 1; i <= limit; i++) {
            System.out.print(fibonacci.calculate(i) + " ");
        }
    }
}
