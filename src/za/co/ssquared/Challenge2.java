package za.co.ssquared;

/**
 * Challenge 2:
 *
 * The "Fizz-Buzz test" is a programming question is as follows:
 *
 * Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number
 * and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”."
 */
public class Challenge2 {

    public static void main(String[] args) {
        // write your code here for "Challenge 2".
    }
}
